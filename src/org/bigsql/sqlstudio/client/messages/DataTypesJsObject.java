/*
 * SQL Studio
 *
 * Copyright (c) 2014, BigSQL.
 * Portions Copyright (c) 2013 - 2014, Open Source Consulting Group, Inc.
 * Portions Copyright (c) 2012 - 2013, StormDB, Inc.
 *
 */
package org.bigsql.sqlstudio.client.messages;

import com.google.gwt.core.client.JavaScriptObject;

public class DataTypesJsObject extends JavaScriptObject {

	protected DataTypesJsObject() {
    }

    public final native String getId() /*-{ return this.id}-*/;

    public final native String getTypeName() /*-{ return this.name }-*/;
    public final native String getHasLength() /*-{ return this.has_length }-*/;
    public final native String getUsageCount() /*-{ return this.usage_count }-*/;
}
