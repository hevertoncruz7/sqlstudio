/*
 * SQL Studio
 *
 * Copyright (c) 2014, BigSQL.
 * Portions Copyright (c) 2013 - 2014, Open Source Consulting Group, Inc.
 * Portions Copyright (c) 2012 - 2013, StormDB, Inc.
 *
 */
package org.bigsql.sqlstudio.client.messages;

import com.google.gwt.core.client.JavaScriptObject;

public class IndexJsObject extends JavaScriptObject {

	protected IndexJsObject() {
    }


    public final native String getId() /*-{ return this.id}-*/;

    public final native String getName() /*-{ return this.name }-*/;
    public final native String getOwner() /*-{ return this.owner }-*/;
    public final native String getAccessMethod() /*-{ return this.access_method }-*/;
    public final native String getPrimaryKey() /*-{ return this.primary_key }-*/;
    public final native String getUnique() /*-{ return this.unique }-*/;
    public final native String getExclusion() /*-{ return this.exclusion }-*/;
    public final native String getPartial() /*-{ return this.partial }-*/;
    public final native String getDefinition() /*-{ return this.definition }-*/;

}
