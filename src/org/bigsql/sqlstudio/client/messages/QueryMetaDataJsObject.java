/*
 * SQL Studio
 *
 * Copyright (c) 2014, BigSQL.
 * Portions Copyright (c) 2013 - 2014, Open Source Consulting Group, Inc.
 * Portions Copyright (c) 2012 - 2013, StormDB, Inc.
 *
 */
package org.bigsql.sqlstudio.client.messages;

import com.google.gwt.core.client.JavaScriptObject;

public class QueryMetaDataJsObject extends JavaScriptObject {

	protected QueryMetaDataJsObject() {
    }

    public final native String getName() /*-{ return this.name }-*/;

    public final native int getDataType() /*-{ return this.data_type }-*/;
}
