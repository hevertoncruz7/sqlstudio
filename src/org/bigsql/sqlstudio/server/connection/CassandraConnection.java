/*
 * SQL Studio
 * 
 * Copyright (c) 2014, BigSQL.
 * Copyright (c) 2013 - 2014, Open Source Consulting Group, Inc.
 * Copyright (c) 2012 - 2013, StormDB, Inc.
 * 
 * Permission to use, copy, modify, and distribute this software and its
 * documentation for any purpose, without fee, and without a written agreement is
 * hereby granted, provided that the above copyright notice and this paragraph and
 * the following two paragraphs appear in all copies.
 * 
 * IN NO EVENT SHALL OPEN SOURCE CONSULTING GROUP BE LIABLE TO ANY PARTY FOR
 * DIRECT, INDIRECT, SPECIAL, INCIDENTAL, OR CONSEQUENTIAL DAMAGES, INCLUDING LOST
 * PROFITS, ARISING OUT OF THE USE OF THIS SOFTWARE AND ITS DOCUMENTATION, EVEN IF
 * OPEN SOURCE CONSULTING GROUP HAS BEEN ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 * 
 * OPEN SOURCE CONSULTING GROUP SPECIFICALLY DISCLAIMS ANY WARRANTIES, INCLUDING,
 * BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A
 * PARTICULAR PURPOSE. THE SOFTWARE PROVIDED HEREUNDER IS ON AN "AS IS" BASIS, AND
 * OPEN SOURCE CONSULTING GROUP HAS NO OBLIGATIONS TO PROVIDE MAINTENANCE, SUPPORT,
 * UPDATES, ENHANCEMENTS, OR MODIFICATIONS.
 * 
 */
package org.bigsql.sqlstudio.server.connection;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

import org.bigsql.sqlstudio.shared.DatabaseConnectionException;
import org.bigsql.sqlstudio.shared.DatabaseConnectionException.DATABASE_ERROR_TYPE;

/**
 * 
 * <pre>
 * Java File.
 * Title        : CassandraConnection.java
 * Description  : Cassandra database connection implementation
 * @author      : Chalimadugu Ranga Reddy
 * @version     : 1.0 13-Oct-2014
 * </pre>
 */
public class CassandraConnection extends AbstractDatabaseConnection
{

	/*
	 * Don't let anyone instantiate this class
	 */
	private CassandraConnection()
	{}

	/*
	 * Class instance
	 */
	private static CassandraConnection instance = null;

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * org.bigsql.sqlstudio.server.connection.ConnectionManager#getConnection
	 * (java.lang.String, java.lang.String, java.lang.String)
	 */
	public Connection getConnection(String token, String clientIP,
			String userAgent) throws DatabaseConnectionException
	{
		ConnectionInfo info = connTable.get(token);

		Connection conn = info.getConnection(token, clientIP, userAgent);

		/* See if it got closed on us and retry creating */
		if (conn != null)
		{
			try
			{
				if (conn.isClosed())
				{
					conn = null;
				}
			}
			catch (SQLException e)
			{
				// ignore
				conn = null;
			}
		}

		if (conn == null)
		{
			try
			{
				conn = createConnection(info.getDatabaseURL(),
						info.getDatabasePort(), info.getDatabaseName(),
						info.getUser(), info.getPassword());
			}
			catch (DatabaseConnectionException e)
			{
				throw new DatabaseConnectionException(
						"Disconnected and could not create a connection. Try again.");
			}

			info.setConnection(conn);
		}
		return info.getConnection(token, clientIP, userAgent);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * org.bigsql.sqlstudio.server.connection.ConnectionManager#createConnection
	 * (java.lang.String, int, java.lang.String, java.lang.String,
	 * java.lang.String)
	 */

	public Connection createConnection(String databaseURL, int databasePort,
			String databaseName, String user, String password)
			throws DatabaseConnectionException
	{

		Connection conn = null;
		String url = null;

		try
		{
			url = "jdbc:cassandra://" + databaseURL + ":"
					+ Integer.toString(databasePort) + "/" + databaseName;

			if (user != null && !"".equals(user) && password != null
					&& !"".equals(password))
			{
				conn = DriverManager.getConnection(url, user, password);
			}
			else
			{
				conn = DriverManager.getConnection(url);
			}

		}
		catch (SQLException e)
		{
			e.printStackTrace();
		}

		if (conn == null)
		{
			throw new DatabaseConnectionException(
					"Unable to connect to Database server",
					DATABASE_ERROR_TYPE.INVALID_CONNECTION_STRING);
		}

		return conn;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * org.bigsql.sqlstudio.server.connection.ConnectionManager#addConnection
	 * (java.lang.String, int, java.lang.String, java.lang.String,
	 * java.lang.String, java.lang.String, java.lang.String)
	 */
	public String addConnection(String databaseURL, int databasePort,
			String databaseName, String user, String password, String clientIP,
			String userAgent) throws DatabaseConnectionException
	{
		Connection conn = null;
		ConnectionInfo info = null;
		String token = null;

		try
		{
			conn = createConnection(databaseURL, databasePort, databaseName,
					user, password);

			info = new ConnectionInfo(conn, clientIP, userAgent, databaseURL,
					databasePort, databaseName, user, password);

			// DBVersionCheck check = new DBVersionCheck(conn);
			// this.databaseVersion = check.getVersion();

			/* TODO : Needs to get it in more formal way. */
			this.databaseVersion = 2;

			info.setDatabaseVersion(databaseVersion);
			token = info.getToken();
			connTable.put(token, info);
		}
		catch (Exception e)
		{
			throw new DatabaseConnectionException(e.getMessage());
		}

		return token;
	}

	/*
	 * Static block to load database specific driver
	 */
	static
	{
		try
		{
			Class.forName("org.bigsql.cassandra2.jdbc.CassandraDriver");
		}
		catch (ClassNotFoundException e)
		{
			e.printStackTrace();
		}

	}

	/**
	 * Method returns instance of DatabaseConnection
	 * 
	 * @return DatabaseConnection
	 */
	public static DatabaseConnection getInstance()
	{
		return (instance == null ? new CassandraConnection() : instance);
	}
}
